import { Router } from "express";

const reviews = [
  {
    id: 1,
    productId: 76,
    stars: 5,
    text: 'It is a f***ing awesome game!',
  },
  {
    id: 2,
    productId: 76,
    stars: 1,
    text: 'I hate it.',
  },
  {
    id: 3,
    productId: 54,
    stars: 3,
    text: 'So, so...',
  },
  {
    id: 4,
    productId: 11,
    stars: 2,
    text: 'I will not buy it again. Waste of money.',
  },
];

const getNewReviewId = () =>  reviews.length ? ++reviews[reviews.length - 1].id : 1;

export const reviewsRoutes = Router()

  .get('/:productId', (req, res) => {
    let { productId } = req.params;
    const productReviews = reviews.filter(review => review.productId === +productId);
    res.send(productReviews)
  })

  .post('/', (req, res) => {
    let { productId, stars, text} = req.body;
    const newReview = { id: getNewReviewId(), productId, stars, text}
    reviews.push(newReview)
    res.send(newReview)
  })
