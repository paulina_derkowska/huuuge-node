import { Router } from "express";

export const categoriesRoutes = Router()
  .get("/", (req, res) => {
    res.send("categories");
  });
