import express, { RequestHandler, Request, Response, NextFunction } from 'express'
import routes from './routes';

const app = express();

app.use(express.json({}));

app.use(routes);

app.listen(8080);
